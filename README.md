# Тестовое задание

## Запуск
```
 – git clone https://frammmm@bitbucket.org/frammmm/contact-test.git
```
### Запуск сервера
```
 - cd contact-test\contact-server
 - yarn
 - yarn start
```

### Запуск фронтенда
```
 – cd contact-test\contact-front
 – yarn
 – yarn start
```

#### Данные для входа менеджера
```
 - login: manager@mail.com
 - password: 123
```